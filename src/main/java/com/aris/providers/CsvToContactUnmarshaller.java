package com.aris.providers;

import com.aris.model.Contact;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public class CsvToContactUnmarshaller implements MessageBodyReader<Contact> {
    @Override
    public boolean isReadable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return aClass.isAssignableFrom(Contact.class);
    }

    @Override
    public Contact readFrom(Class<Contact> aClass, Type type, Annotation[] annotations, MediaType mediaType,
                            MultivaluedMap<String, String> multivaluedMap, InputStream inputStream)
            throws IOException, WebApplicationException {

        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        String csv = in.readLine();
        String[] args = csv.split(",");

        Contact contact = new Contact();
        try {
            contact.setId(new Integer(args[0]));
        } catch (NumberFormatException nfe) {
        }
        contact.setName(args[1]);
        contact.setGender(args[2]);
        contact.setEmail(args[3]);
        contact.setPhone(args[4]);
        contact.setCity(args[5]);
        contact.setCountry(args[6]);
        return contact;
    }
}
