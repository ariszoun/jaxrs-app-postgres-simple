package com.aris.providers;

import com.aris.model.Contact;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;

@Provider
@Produces("text/csv")
public class ArrayListToCsvMarshaller implements MessageBodyWriter<ArrayList<Contact>> {
    @Override
    public boolean isWriteable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return aClass.isAssignableFrom(ArrayList.class);
    }

    @Override
    public long getSize(ArrayList<Contact> contacts, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return -1;
    }

    @Override
    public void writeTo(ArrayList<Contact> contactsList, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> multivaluedMap, OutputStream outputStream) throws IOException, WebApplicationException {
        outputStream.write("Id,Name,Gender,Email,Phone,City,Country\n".getBytes()); //headers
        //data type is ArrayList needs iteration and then get each
        for(Contact contact: contactsList) {
            String out = String.format("%d,%s,%s,%s,%s,%s,%s\n",
                    contact.getId(),
                    contact.getName(),
                    contact.getGender(),
                    contact.getEmail(),
                    contact.getPhone(),
                    contact.getCity(),
                    contact.getCountry());
            outputStream.write(out.getBytes());
        }
    }
}
