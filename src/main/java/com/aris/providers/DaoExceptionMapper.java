package com.aris.providers;
/**
 * Must mention this package name to web.xml inside init-param in order to be scanned
 * */

import com.aris.dao.DaoException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Provider
public class DaoExceptionMapper implements ExceptionMapper<DaoException> {
    //Whenever there is a DaoException being called this func will observe
    //all of the handler function inside resources class and convert the
    //exceptions been thrown into Response objects
    @Override
    public Response toResponse(DaoException exception) {
        Map<String, String> error = new HashMap<>();
        error.put("message", exception.getMessage());
        error.put("when", new Date().toString());
        return Response.status(500).entity(error).build();
    }
}
