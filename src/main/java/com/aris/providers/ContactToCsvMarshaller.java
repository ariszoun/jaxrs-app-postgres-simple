package com.aris.providers;

import com.aris.model.Contact;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

@Provider
@Produces("text/csv")
public class ContactToCsvMarshaller implements MessageBodyWriter<Contact> {
    @Override
    public boolean isWriteable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return aClass.isAssignableFrom(Contact.class);
    }

    @Override
    public long getSize(Contact contact, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return -1;
    }

    @Override
    public void writeTo(Contact contact, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType,
                        MultivaluedMap<String, Object> multivaluedMap, OutputStream outputStream)
            throws IOException, WebApplicationException {
        String out = String.format("%d,%s,%s,%s,%s,%s,%s\n", contact.getId(), contact.getName()
                , contact.getGender(), contact.getEmail(), contact.getPhone(), contact.getCity(), contact.getCountry());
        outputStream.write("Id,Name,Gender,Email,Phone,City,Country\n".getBytes()); //headers
        outputStream.write(out.getBytes());
    }
}
