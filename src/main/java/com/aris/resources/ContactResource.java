package com.aris.resources;

import com.aris.dao.ContactsDao;
import com.aris.dao.DaoException;
import com.aris.dao.DaoFactory;
import com.aris.model.Contact;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/contacts")
public class ContactResource {

    private ContactsDao contactsDao;

//    (Oracle tut) If you define a reference variable whose type is an interface,
//    any object you assign to it must be an instance of a class that implements
//    the interface.
//    Init to constructor
    public ContactResource() throws DaoException {
        contactsDao = DaoFactory.getContactsDao();
    }

    @GET
    @Produces({"application/json","text/csv"})
    public Response getAllContacts() throws DaoException {
        return Response.ok(contactsDao.findAll()).build();
    }

    @Path("/{contact_id}")
    @Produces({MediaType.APPLICATION_JSON})
    @GET
    public Response getOneContact(@PathParam("contact_id") Integer id) throws DaoException {
        return Response.ok(contactsDao.findById(id)).build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response addNewContact(Contact contact) throws DaoException {
        contact = contactsDao.addContact(contact);
        return Response.ok(contact).build();
    }

    @Path("/{contact_id}")
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response updateContact(@PathParam("contact_id") Integer id, Contact contact) throws DaoException {
        contact.setId(id);
        contact = contactsDao.updateContact(contact);
        return Response.ok(contact).build();
    }


    @Path("/{contact_id}")
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteContact(@PathParam("contact_id") Integer id) throws DaoException {
        contactsDao.deleteContact(id);
        return Response.ok().build();
    }

}
