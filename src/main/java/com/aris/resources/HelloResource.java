package com.aris.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/hello")
public class HelloResource {

    @GET
    @Produces({"text/plain"})
    public String greet() {
        return "Hello JAX-RS!!";
    }

    @GET
    @Produces({"application/xml"})
    public String greetAsXml() {
        return "<?xml version=\"1.0\"?>\n" +
                "\n" +
                "<greeting>\n" +
                "\t<message>Hello JAX-RS</message>\n" +
                "\t<from>aris</from>\n" +
                "</greeting>";
    }

    @GET
    @Produces({"application/json"})
    public String greetAsJson() {
        return "{\n" +
                "   \"message\": \"Hello json JAX-RS!!\",\n" +
                "   \"from\" : \"Aris\"\n" +
                "}";
    }
}
