package com.aris.dao;


/**
 * Since Interface ContactsDao is used to provide flexibility for different implementations> We're gonna have to create a new instance
 * of JdbcContactsDao (new JdbcContactsDao or anything else is there hibernate etc..) every time. That's a problem and
 * to avoid it DaoFactory is crated (virtual constructor - factory method).
 *
 * ----------------------------------------------------------------------------
 * About Factory method Design Pattern (src: Design Patterns by Gamma et al): |
 * ----------------------------------------------------------------------------
 * The intent of this pattern, according to Design Patterns by Gamma et al, is to:
 * Define an interface for creating an object, but let subclasses decide which
 * class to instantiate. The Factory method allows a class to defer instantiation
 * to subclasses.
 * https://dzone.com/articles/factory-method-design-pattern
 *
 * Also it is a utility class it is not going to be inherited so "final"
 * and "private constructor" for no-instantiation
 *
 * Right now there is only one implementation jdbc.
 * */
public final class DaoFactory {

    private static final String IMPL_TYPE = "jdbc";

    private DaoFactory() {}

    public static ContactsDao getContactsDao() throws DaoException{

        switch(IMPL_TYPE) {
            case "jdbc":
                return new JdbcContactsDao();
            default:
                throw new DaoException("No suitable implementation available");
        }

    }

}
