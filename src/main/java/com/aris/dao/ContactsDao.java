package com.aris.dao;

import com.aris.model.Contact;

import java.util.List;

public interface ContactsDao {

    //CRUD
    public Contact addContact(Contact contact) throws DaoException;
    public Contact findById(Integer id) throws DaoException;
    public Contact updateContact(Contact contact) throws DaoException;
    public void deleteContact(Integer id) throws DaoException;

    //Queries
    public List<Contact> findAll() throws DaoException;
    public List<Contact> findByCity(String city) throws DaoException;
    public List<Contact> findByCountry(String country) throws DaoException;


}
