package com.aris.dao;

import com.aris.model.Contact;
import com.aris.utils.JDBCUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcContactsDao implements ContactsDao {

    private static final String INSERT_INTO_CONTACTS_SQL = "INSERT INTO contacts (name, gender, email, phone, city, country) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_CONTACT_SQL = "UPDATE CONTACTS SET name=?, gender=?, email=?, phone=?, city=?, country=? WHERE id=?";
    private static final String DELETE_CONTACT_SQL = "DELETE FROM CONTACTS WHERE id=?";

    private static final String FIND_BY_ID_SQL = "SELECT * FROM contacts WHERE id=?";
    private static final String FIND_ALL_SQL = "SELECT * FROM contacts";
    private static final String FIND_BY_CITY_SQL = "SELECT * FROM contacts WHERE city=?";
    private static final String FIND_BY_COUNTRY_SQL = "SELECT * FROM contacts WHERE country=?";

    @Override
    public Contact addContact(Contact contact) throws DaoException {
        try(Connection connection = JDBCUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INTO_CONTACTS_SQL, Statement.RETURN_GENERATED_KEYS)
        ) {
            preparedStatement.setString(1, contact.getName());
            preparedStatement.setString(2, contact.getGender());
            preparedStatement.setString(3, contact.getEmail());
            preparedStatement.setString(4, contact.getPhone());
            preparedStatement.setString(5, contact.getCity());
            preparedStatement.setString(6, contact.getCountry());

            preparedStatement.executeUpdate();
            ResultSet keys = preparedStatement.getGeneratedKeys();
            keys.next();
            contact.setId(keys.getInt(1));

        }catch(Exception e){
            throw new DaoException(e);
        }

        return contact;
    }

    @Override
    public Contact updateContact(Contact contact) throws DaoException {
        try(Connection connection = JDBCUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_CONTACT_SQL)
        ) {
            preparedStatement.setString(1, contact.getName());
            preparedStatement.setString(2, contact.getGender());
            preparedStatement.setString(3, contact.getEmail());
            preparedStatement.setString(4, contact.getPhone());
            preparedStatement.setString(5, contact.getCity());
            preparedStatement.setString(6, contact.getCountry());
            preparedStatement.setInt(7, contact.getId());

            int count = preparedStatement.executeUpdate();
            if (count == 0) {
                throw new DaoException("No records updated: INVALID ID - " + contact.getId());
            }

        }catch (Exception e) {
            throw new DaoException(e);
        }

        return contact;
    }

    @Override
    public void deleteContact(Integer id) throws DaoException {

        try(Connection connection = JDBCUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_CONTACT_SQL)
        ) {
            preparedStatement.setInt(1, id);
            int count = preparedStatement.executeUpdate();
            if (count == 0) {
                throw new DaoException("No records deleted: INVALID ID - " + id);
            }
        }catch (Exception e) {
            throw new DaoException(e);
        }

    }

    @Override
    public Contact findById(Integer id) throws DaoException {

        try(Connection connection = JDBCUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_SQL)
        ) {
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                Contact contact = toContact(rs);
                rs.close();
                return contact;
            }
            rs.close();
        }catch (Exception e) {
            throw new DaoException(e);
        }
        return null;

    }

    @Override
    public List<Contact> findAll() throws DaoException {

        List<Contact> contactsList = new ArrayList<>();

        try(Connection connection = JDBCUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_SQL);
            ResultSet rs = preparedStatement.executeQuery() //we know the rs that's why is here
        ) {
            while(rs.next()) {
                Contact contact = toContact(rs);
                contactsList.add(contact);
            }
        }catch (Exception e) {
            throw new DaoException(e);
        }
        return contactsList;
    }

    @Override
    public List<Contact> findByCity(String city) throws DaoException {

        List<Contact> contactsList = new ArrayList<>();

        try(Connection connection = JDBCUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_CITY_SQL)
            //there is a parameter "where city = ?". Cannot execute it here as before
        ) {
            preparedStatement.setString(1, city); //retrieve the "= ?" parameter
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()) {
                Contact contact = toContact(rs);
                contactsList.add(contact);
            }
            rs.close();
        }catch (Exception e) {
            throw new DaoException(e);
        }
        return contactsList;
    }

    @Override
    public List<Contact> findByCountry(String country) throws DaoException {
        List<Contact> contactsList = new ArrayList<>();

        try(Connection connection = JDBCUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_COUNTRY_SQL)
            //there is a parameter "where city = ?". Cannot execute it here as before
        ) {
            preparedStatement.setString(1, country); //retrieve the "= ?" parameter
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()) {
                Contact contact = toContact(rs);
                contactsList.add(contact);
            }
            rs.close();
        }catch (Exception e) {
            throw new DaoException(e);
        }
        return contactsList;
    }

    private Contact toContact(ResultSet rs) throws SQLException {
        Contact contact = new Contact();
        contact.setId(rs.getInt("id"));
        contact.setName(rs.getString("name"));
        contact.setGender(rs.getString("gender"));
        contact.setEmail(rs.getString("email"));
        contact.setPhone(rs.getString("phone"));
        contact.setCity(rs.getString("city"));
        contact.setCountry(rs.getString("country"));
        return contact;
    }
}
