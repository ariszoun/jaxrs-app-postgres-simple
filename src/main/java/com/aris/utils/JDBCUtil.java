package com.aris.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public final class JDBCUtil {

    private final static String url, driver, user, password;

    private JDBCUtil(){}

    //load jdbc.properties file default values into this values
    static {
        ResourceBundle rb = ResourceBundle.getBundle("jdbc"); //only base name
        url = rb.getString("jdbc.url");
        driver = rb.getString("jdbc.driver");
        user = rb.getString("jdbc.user");
        password = rb.getString("jdbc.password");
    }

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(driver);
        return DriverManager.getConnection(url, user, password);
    }

}
